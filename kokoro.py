from twisted.internet import defer, reactor
import traceback

def fatal_error(res):
    print "*** FAILED"
    print res
    reactor.stop()

def start_tests(cases, tman_factory, manage_reactor=True,
                settle_time=0):
    def add_cb(d, i):
        def start_case(_):
            print "*** STARTING", i.__name__
            d2 = defer.Deferred()

            def on_start(_):
                tman = tman_factory()
                def do_cleanup(res):
                    tman.cleanup()
                    return res
                try:
                    d3 = defer.maybeDeferred(i, tman)
                    d3.addBoth(do_cleanup)
                    return d3
                except:
                    traceback.print_exc()
                    tman.cleanup()
                    raise
            d2.addCallback(on_start)
            reactor.callLater(settle_time, d2.callback, None)
            return d2
        d.addCallback(start_case)
        
    d = defer.Deferred()
    for i in cases:
        add_cb(d, i)
    launched = False
    succeeded = [False]
    def on_succ(_):
        print "*** SUCCEEDED"
        succeeded[0] = True
        if manage_reactor and launched:
            reactor.stop()

    d.addCallback(on_succ)
    if manage_reactor:
        d.addErrback(fatal_error)
    d.callback(None)

    if manage_reactor and not succeeded[0]:
        launched = True
        reactor.run()
    else:
        return d


