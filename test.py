from twisted.internet import reactor, defer
from kokoro import *
import uuid

tman_cleanup_calls = [0]
tman_last_uid = [None]
class TestManager(object):
    def __init__(self):
        self.uid = uuid.uuid4()
    def cleanup(self):
        tman_cleanup_calls[0] += 1
        tman_last_uid[0] = self.uid

def call_one(tman):
    assert(isinstance(tman, TestManager))
    assert(tman_cleanup_calls[0] == 0)
    assert(tman.uid != tman_last_uid[0])
    d = defer.Deferred()
    def on_ready(_):
        print "all is good"
    d.addCallback(on_ready)
    reactor.callLater(1, d.callback, None)
    return d
    

def call_two(tman):
    assert(isinstance(tman, TestManager))
    assert(tman_cleanup_calls[0] == 1)
    assert(tman.uid != tman_last_uid[0])
    d = defer.Deferred()
    def on_ready(_):
        print "all is good2"
        #raise Exception("all is bad")
    d.addCallback(on_ready)
    reactor.callLater(1, d.callback, None)
    return d

def call_three(tman):
    assert(isinstance(tman, TestManager))
    assert(tman_cleanup_calls[0] == 2)
    assert(tman.uid != tman_last_uid[0])


cases = [
    call_one,
    call_two,
    call_three,
]

start_tests(cases, TestManager)
#reactor.callLater(0, start_tests, cases, TestManager)

